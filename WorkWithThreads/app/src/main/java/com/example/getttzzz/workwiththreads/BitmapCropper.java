package com.example.getttzzz.workwiththreads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;

import java.util.concurrent.Executor;

public class BitmapCropper {

    private Bitmap mBitmap;
    private Handler handler;
    private int widthBitmap;
    private int heightBitmap;
    private ShowUICallback showUICallback;


    public BitmapCropper(Bitmap pBitmap, int pWidth, int pHeight, ShowUICallback showUICallback){
        this.mBitmap = pBitmap;
        this.widthBitmap = pWidth;
        this.heightBitmap = pHeight;
        this.showUICallback = showUICallback;
        handler = new Handler();
    }

    public BitmapCropper(Bitmap pBitmap, int pWidth, int pHeight){
        this(pBitmap, pWidth, pHeight, null);
    }

    public void doInAnotherThread(Executor executor) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mBitmap = cropBitmap();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showUICallback.show(mBitmap);
                    }
                });
            }
        });
    }

    public Bitmap cropBitmap() {

        final int color = 0xff424242;
        final Paint paint = new Paint();

        Bitmap output = Bitmap.createBitmap(mBitmap.getWidth(), mBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Rect rectSrc_1 = new Rect(0, 0, widthBitmap/2, heightBitmap/2);
        final Rect rectDst_1 = new Rect(0, 0, widthBitmap/2, heightBitmap/2);

        final Rect rectSrc_2 = new Rect(widthBitmap/2, 0, widthBitmap, heightBitmap/2);
        final Rect rectDst_2 = new Rect(widthBitmap/2, 0, widthBitmap, heightBitmap/2);

        final Rect rectSrc_3 = new Rect(0, heightBitmap/2, widthBitmap/2, heightBitmap);
        final Rect rectDst_3 = new Rect(0, heightBitmap/2, widthBitmap/2, heightBitmap);

        final Rect rectSrc_4 = new Rect(widthBitmap/2, heightBitmap/2, widthBitmap, heightBitmap);
        final Rect rectDst_4 = new Rect(widthBitmap/2, heightBitmap/2, widthBitmap, heightBitmap);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);

        canvas.drawRect(rectDst_1, paint);
        canvas.drawRect(rectDst_2, paint);
        canvas.drawRect(rectDst_3, paint);
        canvas.drawRect(rectDst_4, paint);

        canvas.drawBitmap(mBitmap, rectSrc_1, rectDst_4, paint);
        canvas.drawBitmap(mBitmap, rectSrc_2, rectDst_3, paint);
        canvas.drawBitmap(mBitmap, rectSrc_3, rectDst_2, paint);
        canvas.drawBitmap(mBitmap, rectSrc_4, rectDst_1, paint);

        return output;
    }

    public interface ShowUICallback{
         void show(Bitmap bitmap);
    }


}
