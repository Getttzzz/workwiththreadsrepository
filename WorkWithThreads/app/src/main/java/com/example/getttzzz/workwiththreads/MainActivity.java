package com.example.getttzzz.workwiththreads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private ImageView ivMainImage;
    private Bitmap mBitmap;
    private int widthBitmap;
    private int heightBitmap;
    private Executor executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        executor = Executors.newSingleThreadExecutor();
        View.OnClickListener clickListener = new Clicker();
        findViewById(R.id.bt_do_in_ui).setOnClickListener(clickListener);
        findViewById(R.id.bt_do_in_another_thread).setOnClickListener(clickListener);
        ivMainImage = (ImageView) findViewById(R.id.iv_main_image);

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.my_image_3);
        ivMainImage.setImageBitmap(mBitmap);

        widthBitmap = mBitmap.getWidth();
        heightBitmap = mBitmap.getHeight();
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bt_do_in_ui:
                    doInUI();
                    break;
                case R.id.bt_do_in_another_thread:
                    doInAnotherThread();
                    break;
            }
        }
    }

    private void doInAnotherThread() {
        final BitmapCropper bitmapCropper = new BitmapCropper(mBitmap, widthBitmap, heightBitmap, new ShowUICallback());
        bitmapCropper.doInAnotherThread(executor);
    }

    private void doInUI() {
        final BitmapCropper bitmapCropper = new BitmapCropper(mBitmap, widthBitmap, heightBitmap);
        ivMainImage.setImageBitmap(bitmapCropper.cropBitmap());
    }

    private final class ShowUICallback implements BitmapCropper.ShowUICallback {

        @Override
        public void show(Bitmap pBitmap) {
            ivMainImage.setImageBitmap(pBitmap);
        }
    }

}
